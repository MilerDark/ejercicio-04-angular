import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  dateVar!: number;
  constructor() { 
    this.dateVar = (new Date()).getTime();
  }

  ngOnInit(): void {
    
  }

}
